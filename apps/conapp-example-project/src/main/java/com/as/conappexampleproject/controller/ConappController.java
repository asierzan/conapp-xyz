package com.as.conappexampleproject.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConappController {

    @GetMapping("/")
    public String initMessage(){
        return "This is example app";
    }

    @GetMapping("/hello")
    public String sayHello(){
        return "Hello";
    }
}
