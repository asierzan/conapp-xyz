package com.as.conappexampleproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConappExampleProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConappExampleProjectApplication.class, args);
    }

}
